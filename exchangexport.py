import logging
import re
from base64 import b64decode
from getpass import getpass
from os.path import join

import begin
import exchangelib as xc
import keyring

logger = logging.getLogger(__name__)

filesafere = re.compile(r'[\\/:\*\?"<>\|\0\.]+')


def filesafe(stri):
    return filesafere.sub('_', stri)


def get_header(headers, header):
    try:
        return next(x.value for x in headers if x.name == 'Subject')
    except StopIteration:
        return ''


@begin.start
@begin.tracebacks
@begin.logging
def main(username, address, dest_dir):
    # Get the password and log in
    keyring_system = "exchange-autodiscover:{}".format(address)
    password = keyring.get_password(keyring_system, username)
    from_keyring = password is not None
    if not from_keyring:
        password = getpass("Enter your Exchange password: ")

    creds = xc.Credentials(username=username, password=password)
    account = xc.Account(primary_smtp_address=address,
                         credentials=creds,
                         autodiscover=True,
                         access_type=xc.DELEGATE)

    # now we know we can actually use the password successfully, save it
    keyring.set_password(keyring_system, username, password)

    archive = account.root.get_folder_by_name('Archive')
    for email in archive.all():
        archive_email(email, dest_dir)

    sent = account.root.get_folder_by_name('Sent Items')
    for email in sent.all():
        archive_email(email, dest_dir)


def archive_email(email, dest_dir):
    # This sometimes happens, I have no idea why, so best to leave it as is
    if email.headers is None:
        return

    timestamp = email.datetime_created.strftime('%Y-%m-%dT%H:%M:%S%z')
    msgid = filesafe(
        next(x.value for x in email.headers if x.name == 'Message-ID'))
    subject = filesafe(get_header(email.headers, 'Subject'))
    filename = "{} {} {}".format(timestamp, msgid, subject)[:251] + '.eml'
    with open(join(dest_dir, filename), 'xb') as f:
        f.write(b64decode(email.mime_content))

    email.move_to_trash()
