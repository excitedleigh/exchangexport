from setuptools import setup

setup(
    name='exchangexport',
    description='My awesome project',
    url='https://example.com/',
    author='Adam Brenecki',
    author_email='abrenecki@cmv.com.au',
    license='Proprietary',
    setup_requires=["setuptools_scm>=1.11.1"],
    use_scm_version=True,
    py_modules=['exchangexport'],

    install_requires=[
        'begins',
        'exchangelib',
        'keyring',
    ],
    extras_require={
        'dev': [
            'pytest',
            'prospector',
        ]
    },
    entry_points={
        'console_scripts': ['exchangexport=exchangexport:main.start'],
    },
)
